//
//  PanelButton.swift
//  TheSet
//
//  Created by Evgeny Lee on 19.10.2020.
//

import UIKit

class PanelButton: UIButton {
    
    var isOn = true {
        didSet {
            backgroundColor = isOn ? .isOnButtonColor : .isOffButtonColor
        }
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(frame: .zero)
        backgroundColor = .clear
        initialize()
    }
    
    func initialize() {
        backgroundColor = .isOnButtonColor
        titleLabel?.font = UIFont.systemFont(ofSize: 20.0)
        setTitleColor(.black, for: .normal)
        let width: CGFloat = 44.0
        layer.cornerRadius = width / 2.0
        snp.makeConstraints { make in
            make.width.equalTo(width)
            make.height.equalTo(width)
        }
    }

}
