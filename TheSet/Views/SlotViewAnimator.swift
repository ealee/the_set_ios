//
//  SlotViewAnimator.swift
//  TheSet
//
//  Created by Evgeny Lee on 08.10.2020.
//

import RxSwift
import RxCocoa

struct SlotViewAnimator {
    static func animateCard(for card: UIImageView, to frame: CGRect, with sound: Sound?  = nil) -> Observable<Bool> {
        return Observable.create {[weak card] observer in
            if let sound = sound {
                SoundManager.shared.play(sound: sound)
            }
            
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut) {[weak card] in
                card?.frame = frame
            } completion: {[weak card] finished in
                card?.removeFromSuperview()
                observer.on(.next(finished))
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
    
    static func dropCards(for cards: [UIImageView], to frame: CGRect) -> Observable<Bool> {
        var animations = [Observable<Bool>]()
        for card in cards {
            animations.append(SlotViewAnimator.animateCard(for: card, to: frame))
        }
        return Observable.combineLatest(animations).map {_ in true}
    }
    
    static func dropCards(in mainView: UIView, for slotViews: [SlotView], to scoreView: ScoreView) -> Observable<Bool> {
        var imageViews = [UIImageView]()
        for slotView in slotViews {
            let cardView = slotView.cardView
            let imageView = UIImageView(image: cardView.asImage())
            imageView.contentMode = .scaleAspectFit
            imageView.frame  = cardView.convert(cardView.frame, to: mainView)
            slotView.model = nil
            mainView.addSubview(imageView)
            imageViews.append(imageView)
        }
        
        let frame = scoreView.convert(scoreView.imageView.frame, to: mainView)
        
        return SlotViewAnimator.dropCards(for: imageViews, to: frame)
    }
    
    static func popCards(in mainView: UIView, from deckView: DeckView, to slotViews: [SlotView], with slots: [SlotModel]) -> Observable<(SlotView, SlotModel)> {
        var animations = [Observable<(SlotView, SlotModel)>]()
        var lastCard: UIImageView?
        for slot in slots {
            let slotView = slotViews[slot.slotNumber]
            if slot.card == nil {
                animations.append(Observable.just((slotView, slot)))
                continue
            }
            
            let card = UIImageView(image: CardBackView().asImage())
            card.contentMode = .scaleAspectFit
            card.frame = deckView.convert(deckView.cardBackView.frame, to: mainView)
            if let lCard = lastCard {
                mainView.insertSubview(card, belowSubview: lCard)
                lastCard = card
            } else {
                mainView.addSubview(card)
                lastCard = card
            }
            
            let frame = slotView.convert(slotView.cardView.frame, to: mainView)
            let animation = SlotViewAnimator.animateCard(for: card, to: frame, with: .cardPop).map { _ in (slotView, slot)}
            animations.append(animation)
        }
        
        return Observable.concat(animations)
    }
}
