//
//  PathMaker.swift
//  TheSet
//
//  Created by Evgeny Lee on 06.10.2020.
//

import UIKit

class PathMaker {
    static func createOvalPath(in rect: CGRect) -> UIBezierPath {
        return UIBezierPath(roundedRect: rect, cornerRadius: rect.height / 2.0)
    }
    
    static func createRhombusPath(in rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        let width = rect.size.width
        let height = rect.size.height
        path.move(to: CGPoint(x: 0, y: height / 2.0))
        path.addLine(to: CGPoint(x: width / 2.0, y: height))
        path.addLine(to: CGPoint(x: width, y: height / 2.0))
        path.addLine(to: CGPoint(x: width / 2.0, y: 0))
        path.close()
        return path
    }
    
    static func createCurvePath(in rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        let width = rect.size.width
        let height = rect.size.height
        let arcRadius = height / 3.0
        path.move(to: CGPoint(x: arcRadius, y: arcRadius / 2.0))
        
        path.addCurve(to: CGPoint(x: width - arcRadius, y: arcRadius / 2.0),
            controlPoint1: CGPoint(x: width * (2.0 / 5.0), y: 0),
            controlPoint2: CGPoint(x: width * (3.0 / 5.0), y: height / 2.0))
        
        path.addArc(withCenter: CGPoint(x: width - arcRadius, y: height / 2.0),
                    radius: arcRadius ,
                    startAngle: CGFloat(1.5 * Double.pi),
                    endAngle: CGFloat(Double.pi / 2),
                    clockwise: true)
        
        path.addCurve(to: CGPoint(x: arcRadius, y: height - (arcRadius / 2.0)),
            controlPoint1: CGPoint(x: width * (3.0 / 5.0), y: height),
            controlPoint2: CGPoint(x: width * (2.0 / 5.0), y: height / 2.0))
        
        path.addArc(withCenter: CGPoint(x: arcRadius, y: height / 2.0),
                    radius: arcRadius ,
                    startAngle: CGFloat(Double.pi / 2),
                    endAngle: CGFloat(1.5 * Double.pi),
                    clockwise: true)
        
        path.close()

        return path
    }
    
    static func createShadedPath(in rect: CGRect) -> UIBezierPath {
        let step: CGFloat = 4.0
        
        let path = UIBezierPath()
        
        var x: CGFloat = 0.0
        var y: CGFloat = 0.0
        path.move(to: CGPoint(x: x, y: y))
        
        let count = Int(rect.width / step)
        
        for _ in 0...count {
            y = y > 0.0 ? 0.0 : rect.height
            path.addLine(to: CGPoint(x: x, y: y))
            
            x += step
            path.addLine(to: CGPoint(x: x, y: y))
        }

        return path
    }
}
