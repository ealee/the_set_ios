//
//  BaseView.swift
//  TheSet
//
//  Created by Evgeny Lee on 02.10.2020.
//

import UIKit

class BaseView: UIView {
    @available(*, unavailable)
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init() {
        super.init(frame: .zero)
        backgroundColor = .clear
        initialize()
    }

    //Override in subclasses
    func initialize() {
        
    }
}

