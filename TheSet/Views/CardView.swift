//
//  CardView.swift
//  TheSet
//
//  Created by Evgeny Lee on 05.10.2020.
//

import UIKit

class CardView: BaseView {
    var model: CardModel? {
        didSet {
            updateFiguresView()
        }
    }
    
    private var figuresView: UIView?
    
    override func initialize() {
        backgroundColor = .white
        layer.cornerRadius = CardParameters.cardCornerRadius
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.clear.cgColor
    }
    
    private func updateFiguresView() {
        figuresView?.removeFromSuperview()
        figuresView = nil
        
        guard let card = model else {
            return
        }
        
        let figures = Figures.figures(for: card)
        addSubview(figures)
        figures.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        figures.setShadow(offset: CGSize(width: 1.0, height:  1.0), radius: 2.0, opacity: 0.5, color: .black)
        
        figures.layer.shouldRasterize = true
        figuresView = figures
    }
}
