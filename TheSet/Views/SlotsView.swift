//
//  SlotsView.swift
//  TheSet
//
//  Created by Evgeny Lee on 02.10.2020.
//

import UIKit

class SlotsView: BaseView {
    var slotViews = [SlotView]()
    
    private let mainStackView = UIStackView()
    
    override func initialize() {
        fitView(mainStackView)
        mainStackView.axis = .vertical
        mainStackView.spacing = 10.0
        mainStackView.distribution = .fillEqually
        
        for _ in 1...4 {
            let rowView = UIStackView()
            rowView.spacing = 10.0
            rowView.distribution = .fillEqually
            rowView.snp.makeConstraints { make in
                make.height.equalTo(CardParameters.cardSize.height)
            }
            
            mainStackView.insertArrangedSubview(rowView, at: 0)
            
            for _ in 1...4 {
                let slotView = SlotView()
                rowView.insertArrangedSubview(slotView, at: 0)
                slotViews.append(slotView)
                
                let shadowView = ShadowView()
                slotView.shadowView = shadowView
                insertSubview(shadowView, belowSubview: mainStackView)
                shadowView.snp.makeConstraints { make in
                    make.edges.equalTo(slotView.cardView)
                }
                slotView.model = nil
            }
        }
    }
}
