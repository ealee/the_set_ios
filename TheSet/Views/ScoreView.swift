//
//  ScoreView.swift
//  TheSet
//
//  Created by Evgeny Lee on 05.10.2020.
//

import UIKit

class ScoreView: BaseView {
    let label = UILabel()
    let imageView = UIImageView()
    
    override func initialize() {
        let stackView = UIStackView()
        stackView.spacing = 2.0
        fitView(stackView)
        
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.textColor = .scoreColor
        label.text = "0"
        stackView.addArrangedSubview(label)
        
        let labelX = UILabel()
        labelX.font = UIFont.systemFont(ofSize: 12.0)
        labelX.textColor = .scoreColor
        labelX.text = "X"
        stackView.addArrangedSubview(labelX)
        
        imageView.contentMode = .scaleAspectFit
        imageView.image = CardBackView().asImage()
        let kSize = CardParameters.cardSize
        let k: CGFloat = 44.0 / kSize.height
        let size = CGSize(width: kSize.width * k, height: kSize.height * k)
        imageView.snp.makeConstraints { make in
            make.size.equalTo(size)
        }
        stackView.addArrangedSubview(imageView)
        setShadow(offset: CGSize.zero, radius: 3.0, opacity: 0.7, color: .black)
    }
}

