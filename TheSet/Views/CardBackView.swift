//
//  CardBackView.swift
//  TheSet
//
//  Created by Evgeny Lee on 08.10.2020.
//

import UIKit

class CardBackView: BaseView {
    override func initialize() {
        let size = CardParameters.cardSize
        frame = CGRect(origin: CGPoint.zero, size: size)
        layer.cornerRadius = CardParameters.cardCornerRadius
        backgroundColor = .cardBackColor
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.white.cgColor
        setShadow(offset: CGSize(width: 3.0, height:  3.0), radius: 8.0, opacity: 0.5, color: .black)
        
        addOval(in: CGRect(x: size.width / 4.0,
                       y: size.height / 4.0,
                       width: size.width / 2.0,
                       height: size.height / 2.0))
        
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.italicSystemFont(ofSize: size.width * 0.25)
        label.textAlignment = .center
        label.text = "SET"
        let transform = CGAffineTransform(rotationAngle: Math.deg2rad(90))
        label.transform = transform
        label.setShadow(offset: CGSize(width: 1.0, height:  1.0), radius: 2.0, opacity: 0.5, color: .black)
        label.sizeToFit()
        addSubview(label)
        label.center = center
        label.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
    private func addOval(in rect: CGRect) {
        let ellipsePath = UIBezierPath(ovalIn: rect)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = ellipsePath.cgPath
        shapeLayer.fillColor = UIColor.white.cgColor
        layer.insertSublayer(shapeLayer, at: 0)
    }
}
