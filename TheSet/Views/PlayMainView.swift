//
//  PlayMainView.swift
//  TheSet
//
//  Created by Evgeny Lee on 02.10.2020.
//

import UIKit

class PlayMainView: BaseView {
    let panelView = BaseView()
    let infoButton = PanelButton()
    let soundButton = PanelButton()
    let musicButton = PanelButton()
    let refreshButton = PanelButton()
    let helpButton = PanelButton()
    let deckView = DeckView()
    let scoreView = ScoreView()
    let slotsView = SlotsView()
    
    private let inset: CGFloat = 10.0
    
    override func initialize() {
        backgroundColor = .brandGreen
        clipsToBounds = true
        
        addSubview(panelView)
        panelView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
        
        configureDeck()
        configureScoreView()
        configureSlotsView()
        
        configureButtons()
    }
    
    private func configureButtons() {
        // TODO: Need assets for buttons
        infoButton.setTitle("I", for: .normal)
        panelView.addSubview(infoButton)
        infoButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(inset)
            make.left.equalToSuperview().inset(inset)
        }
        
        soundButton.setTitle("S", for: .normal)
        panelView.addSubview(soundButton)
        soundButton.snp.makeConstraints { make in
            make.top.equalTo(infoButton.snp.bottom).offset(inset)
            make.left.equalToSuperview().inset(inset)
        }
        
        musicButton.setTitle("M", for: .normal)
        panelView.addSubview(musicButton)
        musicButton.snp.makeConstraints { make in
            make.top.equalTo(soundButton.snp.bottom).offset(inset)
            make.left.equalToSuperview().inset(inset)
        }
        
        refreshButton.setTitle("R", for: .normal)
        panelView.addSubview(refreshButton)
        refreshButton.snp.makeConstraints { make in
            make.top.equalTo(scoreView.snp.bottom).offset(inset)
            make.right.equalToSuperview().inset(inset)
        }
        
        helpButton.setTitle("H", for: .normal)
        panelView.addSubview(helpButton)
        helpButton.snp.makeConstraints { make in
            make.top.equalTo(refreshButton.snp.bottom).offset(inset)
            make.right.equalToSuperview().inset(inset)
        }
    }
    
    private func configureDeck() {
        panelView.addSubview(deckView)
        deckView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(inset)
            make.centerX.equalToSuperview()
        }
    }
    
    private func configureScoreView() {
        panelView.addSubview(scoreView)
        scoreView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(inset)
            make.right.equalToSuperview().inset(inset)
        }
    }
    
    private func configureSlotsView() {
        addSubview(slotsView)
        slotsView.snp.makeConstraints { make in
            make.top.equalTo(panelView.snp.bottom)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom).inset(inset)
        }
    }
}
