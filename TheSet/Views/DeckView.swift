//
//  DeckView.swift
//  TheSet
//
//  Created by Evgeny Lee on 05.10.2020.
//

import UIKit

class DeckView: BaseView {
    let cardBackView = CardBackView()
    
    override func initialize() {
        fitView(cardBackView)
        
        setShadow(offset: CGSize(width: 3.0, height:  3.0), radius: 8.0, opacity: 0.5, color: .black)
        
        let size = CardParameters.cardSize
        snp.makeConstraints { make in
            make.width.equalTo(size.width)
            make.height.equalTo(size.height)
        }
    }
}
