//
//  ShadowView.swift
//  TheSet
//
//  Created by Evgeny Lee on 07.10.2020.
//

import UIKit

class ShadowView: BaseView {
    override func initialize() {
        backgroundColor = .black
        layer.cornerRadius = CardParameters.cardCornerRadius
        layer.masksToBounds = false
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize(width: 3.0, height:  3.0 )
        layer.shadowOpacity = 0.5
        layer.shadowColor = UIColor.black.cgColor
        layer.shouldRasterize = true
    }
}
