//
//  Figures.swift
//  TheSet
//
//  Created by Evgeny Lee on 05.10.2020.
//

import UIKit

class Figures {
    static let figureRect = CGRect(x: 0.0,
                      y: 0.0,
                      width: CardParameters.cardSize.width * 0.8,
                      height: CardParameters.cardSize.height * 0.2)
    
    static func figures(for card: CardModel) -> UIView {
        let view = UIView()
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 5.0
        stackView.distribution = .fillEqually
        view.fitView(stackView)
        
        for _ in 0..<card.count.rawValue {
            stackView.addArrangedSubview(Figures.figure(for: card))
        }
        
        return view
    }
    
    static func figure(for card: CardModel) -> UIView {
        let rect = Figures.figureRect
        let view = UIView()
        view.snp.makeConstraints { make in
            make.width.equalTo(rect.width)
            make.height.equalTo(rect.height)
        }
        
        let color = Figures.color(for: card.color)
        
        let path = Figures.path(in: rect, for: card.figure)
        let fillColor = card.fillness == .variant3 ? color : .clear
        
        if card.fillness == .variant2 {
            let shadedLayer = Figures.shadedLayer(in: rect, path: path, color: color)
            view.layer.addSublayer(shadedLayer)
        }
        
        let shapeLayer = Figures.createLayer(in: rect, fillColor: fillColor, strokeColor: color)
        shapeLayer.path = path.cgPath
        view.layer.addSublayer(shapeLayer)
        
        return view
    }
    
    static func color(for variant: Variant) -> UIColor {
        switch variant.rawValue {
        case 1:
            return .color1
        case 2:
            return .color2
        case 3:
            return .color3
        default:
            return .color1
        }
    }
    
    static func createLayer(in rect: CGRect, fillColor: UIColor = .black, strokeColor: UIColor = .black) -> CAShapeLayer {
        let layer = CAShapeLayer()
        layer.frame = rect
        layer.strokeColor = strokeColor.cgColor
        layer.fillColor = fillColor.cgColor
        layer.lineWidth = 2.0
        return layer
    }
    
    static func path(in rect: CGRect, for variant: Variant) -> UIBezierPath {
        switch variant.rawValue {
        case 1:
            return PathMaker.createOvalPath(in: rect)
        case 2:
            return PathMaker.createRhombusPath(in: rect)
        case 3:
            return PathMaker.createCurvePath(in: rect)
        default:
            return UIBezierPath()
        }
    }
    
    static func shadedLayer(in rect: CGRect, path: UIBezierPath, color: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        layer.masksToBounds = true
        
        layer.backgroundColor = UIColor.white.cgColor
        layer.frame = rect
        layer.fillColor = nil
        layer.strokeColor = color.cgColor
        layer.lineWidth = 2.0
        layer.path = PathMaker.createShadedPath(in: rect).cgPath
        
        return layer
    }
}
