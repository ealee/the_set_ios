//
//  SlotView.swift
//  TheSet
//
//  Created by Evgeny Lee on 05.10.2020.
//

import UIKit
import SnapKit

class SlotView: BaseView {
    let cardView = CardView()
    
    var isSelected = false {
        didSet {
            let offset: CGFloat = isSelected ? 8.0 : 0.0
            cardViewBottomConstraint?.update(inset: offset)
        }
    }
    var isHighLighted = false {
        didSet {
            cardView.layer.borderColor = isHighLighted ? UIColor.highLight.cgColor : UIColor.clear.cgColor
        }
    }
    var model: SlotModel? {
        didSet {
            var isHidden = false
            if model?.card == nil {
                isHidden = true
                isSelected = false
                isHighLighted = false
            }
            cardView.isHidden = isHidden
            shadowView?.isHidden = isHidden
            cardView.model = model?.card
        }
    }
    weak var shadowView: ShadowView?
    
    private var cardViewBottomConstraint: Constraint?
    
    override func initialize() {
        addSubview(cardView)

        let size = CardParameters.cardSize
        cardView.snp.makeConstraints { make in
            cardViewBottomConstraint = make.bottom.equalToSuperview().constraint
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.width.equalTo(size.width)
            make.height.equalTo(size.height)
        }
    }
}
