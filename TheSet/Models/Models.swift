//
//  Models.swift
//  TheSet
//
//  Created by Evgeny Lee on 22.10.2020.
//

import Foundation

enum Variant: Int, CaseIterable {
    case variant1 = 1
    case variant2
    case variant3
}

enum GameState {
    case refresh
    case help
    case add
    case set
    case select
    case deselect
}

struct CardModel: Equatable {
    let count: Variant
    let figure: Variant
    let color: Variant
    let fillness: Variant
        
    init?(with array: [Variant]) {
        if array.count != 4 {
            return nil
        }
        count = array[0]
        figure = array[1]
        color = array[2]
        fillness = array[3]
    }
}

struct SlotModel: Equatable {
    let slotNumber: Int
    var card: CardModel?
}

struct StateModel {
    let state: GameState
    let cardsLeft: Int
    let score: Int
    let slots: [SlotModel]
}
