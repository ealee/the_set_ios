//
//  PlayViewController.swift
//  TheSet
//
//  Created by Evgeny Lee on 02.10.2020.
//

import RxSwift
import RxCocoa

class PlayViewController: UIViewController {
    private let mainView = PlayMainView()
    
    let disposeBag = DisposeBag()
    let viewModel = PlayViewModel()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeView()
        configureView()
        bindViewModel()
    }
    
    // MARK: - Private
    
    private func localizeView() {
        
    }
    
    private func configureView() {
        view.fitView(mainView)
        mainView.soundButton.isOn = SoundManager.shared.isSoundEnabled
        mainView.musicButton.isOn = SoundManager.shared.isMusicEnabled
    }
    
    private func bindViewModel() {
        mainView.infoButton.rx.tap.bind {[weak self] _ in
            let controller = UIAlertController(title: nil, message: GameRules.text, preferredStyle: .alert)
            controller.addAction(UIAlertAction(title:"Ok", style: .default))
            self?.show(controller, sender: nil)
        }.disposed(by: disposeBag)
        
        mainView.soundButton.rx.tap.bind {[weak self] _ in
            SoundManager.shared.isSoundEnabled = !SoundManager.shared.isSoundEnabled
            self?.mainView.soundButton.isOn = SoundManager.shared.isSoundEnabled
        }.disposed(by: disposeBag)
        
        mainView.musicButton.rx.tap.bind {[weak self] _ in
            SoundManager.shared.isMusicEnabled = !SoundManager.shared.isMusicEnabled
            self?.mainView.musicButton.isOn = SoundManager.shared.isMusicEnabled
        }.disposed(by: disposeBag)
        
        let viewIsReady = rx.sentMessage(#selector(viewDidAppear(_:)))
            .take(1)
            .mapToVoid()
        
        let refreshTrigger = Observable.merge(viewIsReady, mainView.refreshButton.rx.tap.mapToVoid())
        
        let helpTrigger = mainView.helpButton.rx.tap.mapToVoid()
        
        let addCardsTrigger = mainView.deckView.rx.tap().mapToVoid()
        
        let cardViewsTapActions = mainView.slotsView.slotViews.map({ slotView -> Observable<SlotModel?> in
            slotView.rx.tap().map { view -> SlotModel? in
                guard let slotView = view as? SlotView else {
                    return nil
                }
                return slotView.model
            }
        })
        let slotSelectionTrigger = Observable.merge(cardViewsTapActions)
        
        let input = PlayViewModel.Input(refreshTrigger: refreshTrigger,
                                        helpTrigger: helpTrigger,
                                        addCardsTrigger: addCardsTrigger,
                                        slotSelectionTrigger: slotSelectionTrigger)
        
        let output = viewModel.transform(input: input)
        
        output.update
            .drive {[weak self] state in
                self?.update(with: state)
            }
            .disposed(by: disposeBag)
    }
    
    private func dropCards(for slotViews: [SlotView]) -> Driver<Bool> {
        SoundManager.shared.play(sound: .dropSet)
        return SlotViewAnimator.dropCards(in: mainView, for: slotViews, to: mainView.scoreView).asDriver(onErrorJustReturn: false)
    }
    
    private func popCards(for slots: [SlotModel]) {
        cardActions(enabled: false)
        SlotViewAnimator.popCards(in: mainView, from: mainView.deckView, to: mainView.slotsView.slotViews, with: slots)
            .asDriverOnErrorJustComplete()
            .drive (onNext: { (slotView, slot) in
                slotView.model = slot
            }, onCompleted: { [weak self] in
                self?.cardActions(enabled: true)
            })
            .disposed(by: disposeBag)
    }
    
    private func getSlotViews(for slots: [SlotModel]) -> [SlotView] {
        var slotViews = [SlotView]()
        for slot in slots {
            let slotView = mainView.slotsView.slotViews[slot.slotNumber]
            slotViews.append(slotView)
        }
        return slotViews
    }
    
    private func cardActions(enabled: Bool) {
        mainView.deckView.isUserInteractionEnabled = enabled
        mainView.refreshButton.isEnabled = enabled
        mainView.refreshButton.isOn = enabled
        mainView.helpButton.isEnabled = enabled
        mainView.helpButton.isOn = enabled
    }
    
    private func update(with state: StateModel) {
        switch state.state {
        case .refresh:
            SoundManager.shared.play(sound: .refresh)
            mainView.deckView.isHidden = false
            mainView.scoreView.label.text = "\(state.score)"
            mainView.slotsView.slotViews.forEach {$0.model = nil}
            popCards(for: state.slots)
        case .help:
            SoundManager.shared.play(sound: .help)
            let slotViews = getSlotViews(for: state.slots)
            slotViews.forEach{ $0.isHighLighted = true }
        case .add:
            popCards(for: state.slots)
        case .set:
            cardActions(enabled: false)
            let slotViews = getSlotViews(for: state.slots)
            dropCards(for: slotViews).drive {[weak self] _ in
                self?.mainView.scoreView.label.text = "\(state.score)"
                self?.popCards(for: state.slots)
            }.disposed(by: disposeBag)
        case .select, .deselect:
            let isSelected = state.state == .select ? true : false
            SoundManager.shared.play(sound: isSelected ? .cardSelect : .cardDeselect)
            let slotView = getSlotViews(for: state.slots).first
            slotView?.isSelected = isSelected
            slotView?.model = state.slots.first
        }
    }
}
