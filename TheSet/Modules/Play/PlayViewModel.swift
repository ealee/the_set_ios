//
//  PlayViewModel.swift
//  TheSet
//
//  Created by Evgeny Lee on 02.10.2020.
//

import RxSwift
import RxCocoa

class PlayViewModel {
    private let slotsCount = 16
    private let initialSlotsCount = 12
    
    private let disposeBag = DisposeBag()
    private let cardsManager = CardsManager()
    
    private var score = 0
    private var slots = [SlotModel]()
    private var selectedSlots = [SlotModel]()

    private func make(state: GameState, slots: [SlotModel]) -> StateModel {
        return StateModel(state: state, cardsLeft: cardsManager.cards.count, score: score, slots: slots)
    }
    
    private func replaceSlots(with newSlots: [SlotModel]) {
        for slot in newSlots {
            slots.replaceSubrange(slot.slotNumber...slot.slotNumber, with: [slot])
        }
    }
    
    private func refresh() -> StateModel {
        cardsManager.refresh()
        slots.removeAll()
        selectedSlots.removeAll()
        for i in 0..<slotsCount {
            let card = i < initialSlotsCount ? cardsManager.getCard() : nil
            slots.append(SlotModel(slotNumber: i, card: card))
        }
        return make(state: .refresh, slots: slots)
    }
    
    private func findSet() -> StateModel? {
        let indexList = Math.generateCombinations(of: 3, from: 1...12, noRepetitions: true)
        for i in indexList {
            let slotSet = [slots[i[0]], slots[i[1]], slots[i[2]]]
            let cardSet = slotSet.compactMap { $0.card }
            if cardsManager.checkSet(cardSet) {
                return make(state: .help, slots: slotSet)
            }
        }
        return nil
    }
    
    private func addCards() -> StateModel? {
        var slotsToFill = slots.filter({$0.card == nil})
        if slotsToFill.isEmpty {
            return nil
        }
        if slotsToFill.count > 4 {
            slotsToFill = Array(slots[0..<4])
        }
        let newSlots = slotsToFill.map { SlotModel(slotNumber: $0.slotNumber, card: cardsManager.getCard()) }
        replaceSlots(with: newSlots)
        return make(state: .add, slots: newSlots)
    }
    
    private func selection(for slot: SlotModel) -> StateModel? {
        var state = GameState.deselect
        if let index = selectedSlots.firstIndex(of: slot) {
            selectedSlots.remove(at: index)
        } else if selectedSlots.count < 3 {
            selectedSlots.append(slot)
            state = .select
        }
        
        var newSlots = [slot]
        if selectedSlots.count == 3 {
            let selectedCards = selectedSlots.compactMap { $0.card }
            if cardsManager.checkSet(selectedCards) {
                let filledSlotsCount = slots.compactMap { $0.card }.count - 3
                var cardsCount = filledSlotsCount < initialSlotsCount ? initialSlotsCount - filledSlotsCount : 0
                newSlots = selectedSlots.map { slot in
                    cardsCount -= 1
                    return SlotModel(slotNumber: slot.slotNumber, card: cardsCount >= 0 ? cardsManager.getCard() : nil)
                }
                selectedSlots.removeAll()
                state = .set
                score += 1
                replaceSlots(with: newSlots)
            }
        }
        return make(state: state, slots: newSlots)
    }
    
    func transform(input: Input) -> Output {
        let refresh = input.refreshTrigger.map { [weak self] _ in self?.refresh() }
            
        let help = input.helpTrigger.map { [weak self] _ in self?.findSet() }
        
        let addCards = input.addCardsTrigger.map { [weak self] _ in self?.addCards() }
        
        let slotSelection = input.slotSelectionTrigger.compactMap { $0 }.map { [weak self] slot in self?.selection(for: slot) }
        
        let update = Observable<StateModel?>.merge(refresh, help, addCards, slotSelection)
            .compactMap { $0 }
            .asDriverOnErrorJustComplete()
        
        return Output(update: update)
    }
}

extension PlayViewModel {
    struct Input {
        let refreshTrigger: Observable<Void>
        let helpTrigger: Observable<Void>
        let addCardsTrigger: Observable<Void>
        let slotSelectionTrigger: Observable<SlotModel?>
    }
    
    struct Output {
        let update: Driver<StateModel>
    }
}
