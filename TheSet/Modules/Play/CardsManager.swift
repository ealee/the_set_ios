//
//  CardsManager.swift
//  TheSet
//
//  Created by Evgeny Lee on 05.10.2020.
//

import Foundation

class CardsManager {
    var cards = [CardModel]()
    
    private let cardsSequence = CardsManager.cardsSequence()
    
    static private func cardsSequence() -> [CardModel] {
        let combinations = Math.generateCombinations(of: 4, from: 1...3)
        return combinations.compactMap { CardModel(with: $0.compactMap { Variant(rawValue: $0) }) }
    }
    
    func refresh() {
        cards = cardsSequence.shuffled()
    }
    
    func getCard() -> CardModel? {
        var card: CardModel?
        if !cards.isEmpty {
            card = cards.removeFirst()
        }
        return card
    }
    
    func checkSet(_ cardsToCheck: [CardModel]) -> Bool {
        guard cardsToCheck.count == 3 else {
            return false
        }
        
        let sums = [
            cardsToCheck.reduce(0) { $0 + $1.count.rawValue },
            cardsToCheck.reduce(0) { $0 + $1.figure.rawValue },
            cardsToCheck.reduce(0) { $0 + $1.color.rawValue },
            cardsToCheck.reduce(0) { $0 + $1.fillness.rawValue }
        ]
        return sums.reduce(true, { $0 && $1.isMultiple(of: 3) })
    }
}
