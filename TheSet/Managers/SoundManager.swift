//
//  SoundManager.swift
//  TheSet
//
//  Created by Evgeny Lee on 11.10.2020.
//

import Foundation
import AVFoundation

enum Sound: String, CaseIterable {
    case cardSelect
    case cardDeselect
    case cardPop
    case dropSet
    case help
    case refresh
    case finish
}

enum Music: String, CaseIterable {
    case chill = "music_chill"
    case dayum = "music_dayum"
    case cosmicGlow = "music_cosmic_glow"
    case parallelUniverse = "music_parallel_universe"
    case rumours = "music_rumours"
}

class SoundManager: NSObject {
    static let shared = SoundManager()
    
    static private let soundExtension = "wav"
    static private let musicExtension = "mp3"
    
    private var isPrepared = false
    private var players = [AVAudioPlayer]()
    private var soundsCache = [String: Data]()
    private var musicPlayer: AVAudioPlayer?
    
    var isSoundEnabled: Bool = true {
        didSet {
            AppSettings.isSoundEnabled = isSoundEnabled
            if !isSoundEnabled {
                stopSound()
            }
        }
    }
    var isMusicEnabled: Bool = true {
        didSet {
            AppSettings.isMusicEnabled = isMusicEnabled
            if isMusicEnabled {
                if musicPlayer == nil {
                    playMusic()
                }
            } else {
                stopMusic()
            }
        }
    }
    
    override init() {
        super.init()
        
        loadData()
    }
    
    private func loadData() {
        Sound.allCases.forEach { soundName in
            DispatchQueue.global().async() {[weak self] in
                guard let data = self?.load(fileName: soundName.rawValue, fileExtension: SoundManager.soundExtension) else {
                    return
                }
                DispatchQueue.main.async() {[weak self] in
                    self?.soundsCache[soundName.rawValue] = data
                }
            }
        }
    }
    
    private func load(fileName: String, fileExtension: String) -> Data? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: url)
                return data
            } catch {
                Log.error(error.localizedDescription)
                return nil
            }
        } else {
            Log.error("file not found: \(fileName).\(fileExtension)")
            return nil
        }
    }
    
    private func play(data: Data, volume: Float = 1.0) -> AVAudioPlayer? {
        do {
            let player = try AVAudioPlayer(data: data)
            player.volume = volume
            player.delegate = self
            player.play()
            return player
        } catch {
            Log.error(error.localizedDescription)
            return nil
        }
    }
    
    private func play(music: Music) {
        DispatchQueue.global().async() {[weak self] in
            guard let data = self?.load(fileName: music.rawValue, fileExtension: SoundManager.musicExtension) else {
                return
            }
            DispatchQueue.main.async() {[weak self] in
                guard let player = self?.play(data: data, volume: 0.5) else {
                    return
                }
                self?.musicPlayer = player
            }
        }
    }
    
    static func prepare() {
        let manager = SoundManager.shared
        manager.isSoundEnabled = AppSettings.isSoundEnabled
        manager.isMusicEnabled = AppSettings.isMusicEnabled
        manager.isPrepared = true
    }
    
    func play(sound: Sound) {
        guard isPrepared,
              isSoundEnabled,
              let data = soundsCache[sound.rawValue],
              let player = play(data: data) else {
            return
        }
        players.append(player)
    }
    
    func stopSound() {
        players.removeAll()
    }
    
    func playMusic() {
        guard isPrepared,
              isMusicEnabled,
              let musicName = Music.allCases.randomElement() else {
            return
        }
        play(music: musicName)
    }
    
    func stopMusic() {
        musicPlayer = nil
    }
}

extension SoundManager: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if player == musicPlayer {
            playMusic()
        } else {
            players.removeAll(where: {$0 == player})
        }
    }
}
