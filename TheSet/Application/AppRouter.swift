//
//  AppRouter.swift
//  TheSet
//
//  Created by Evgeny Lee on 23.10.2020.
//

import UIKit

class AppRouter {
    static var mainWindow: UIWindow? {
        if #available(iOS 13, *) {
            guard let sceneDelegate = UIApplication.shared.connectedScenes
                    .first?.delegate as? SceneDelegate else {
                return nil
            }
            return sceneDelegate.window
        } else {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return nil
            }
            return appDelegate.window
        }
    }
    
    static func startWindow() -> UIWindow {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let viewController = PlayViewController()
        window.rootViewController = viewController
        return window
    }
}
