//
//  AppSettings.swift
//  TheSet
//
//  Created by Evgeny Lee on 23.10.2020.
//

import Foundation

private enum AppSettingsKeys: String {
    case didFirstLaunch = "AppSettings.didFirstLaunch"
    case isSoundEnabled = "AppSettings.isSoundEnabled"
    case isMusicEnabled = "AppSettings.isMusicEnabled"
}

class AppSettings {
    static var isFirstLaunch: Bool {
        let didFirstLaunch = UserDefaults.standard.bool(forKey: AppSettingsKeys.didFirstLaunch.rawValue)
        if !didFirstLaunch {
            UserDefaults.standard.set(true, forKey: AppSettingsKeys.didFirstLaunch.rawValue)
        }
        return !didFirstLaunch
    }
    
    static var isSoundEnabled: Bool {
        get {
            UserDefaults.standard.bool(forKey: AppSettingsKeys.isSoundEnabled.rawValue)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppSettingsKeys.isSoundEnabled.rawValue)
        }
    }
    
    static var isMusicEnabled: Bool {
        get {
            UserDefaults.standard.bool(forKey: AppSettingsKeys.isMusicEnabled.rawValue)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppSettingsKeys.isMusicEnabled.rawValue)
        }
    }
    
    static func firstLaunch() {
        if isFirstLaunch {
            isSoundEnabled = true
            isMusicEnabled = true
        }
    }
}
