//
//  Colors.swift
//  TheSet
//
//  Created by Evgeny Lee on 02.10.2020.
//

import UIKit

extension UIColor {
    static var brandGreen: UIColor {
        return "#3B8211".hexColor()
    }
    
    static var cardBackColor: UIColor {
        return "#DCB8FF".hexColor()
    }
    
    static var scoreColor: UIColor {
        return .yellow
    }
    
    static var isOnButtonColor: UIColor {
        return .init(white: 1.0, alpha: 0.85)
    }
    
    static var isOffButtonColor: UIColor {
        return .init(white: 1.0, alpha: 0.45)
    }
    
    static var color1: UIColor {
        return "#892996".hexColor()
    }
    
    static var color2: UIColor {
        return "#E41C1C".hexColor()
    }
    
    static var color3: UIColor {
        return "#4CD927".hexColor()
    }
    
    static var highLight: UIColor {
        return .red
    }
}
