//
//  GameRules.swift
//  TheSet
//
//  Created by Evgeny Lee on 23.10.2020.
//

import Foundation

class GameRules {
    static let text = "For each one of the four categories of features — color, number, shape, and shading — the three cards must display that feature as a) either all the same, or b) all different. Put another way: For each feature the three cards must avoid having two cards showing one version of the feature and the remaining card showing a different version. For any \"set\", the number of features that are all the same and the number of features that are all different may break down as 0 the same + 4 different; or 1 the same + 3 different; or 2 the same + 2 different; or 3 the same + 1 different."
}
