//
//  CardParameters.swift
//  TheSet
//
//  Created by Evgeny Lee on 08.10.2020.
//

import UIKit

struct CardParameters {
    static var cardSize: CGSize {
        var width: CGFloat = (UIScreen.main.bounds.width - 50) / 4.0
        var height: CGFloat = width * 1.5
        if height * 4 + 30.0 > UIScreen.main.bounds.height / 1.5 {
            height = (UIScreen.main.bounds.height / 1.5 - 30.0) / 4.0
            width = height / 1.5
        }
        return  CGSize(width: width, height: height)
    }
    
    static var cardCornerRadius: CGFloat {
        return  CardParameters.cardSize.width * 0.1
    }
}
