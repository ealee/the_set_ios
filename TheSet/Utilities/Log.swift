//
//  Log.swift
//  TheSet
//
//  Created by Evgeny Lee on 02.10.2020.
//

import Foundation

class Log {
    
    enum Prefix: String {
        case none = ""
        case debug = "[DEBUG]"
        case warning = "[WARNING]"
        case error = "[ERROR]"
    }

    enum Parameters: String {
        case dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
    }
    
    static func currentTimeStamp() -> String {
        let timestampFormatter = DateFormatter()
        timestampFormatter.dateFormat = Log.Parameters.dateFormat.rawValue
        return timestampFormatter.string(from: Date())
    }
    
    static func message(prefix : Log.Prefix = Log.Prefix.none,
                       object:Any? = nil,
                       file: String = #file,
                       line: Int = #line,
                       function: String = #function) {
        
        
        let prefixString: String
        if prefix.rawValue.isEmpty {
            prefixString = ""
        } else {
            prefixString = " \(prefix.rawValue)"
        }
        
        let trimmedFile: String
        if let lastIndex = file.lastIndex(of: "/") {
            trimmedFile = String(file[file.index(after: lastIndex) ..< file.endIndex])
        }
        else {
            trimmedFile = file
        }
        
        let identifier = "\(trimmedFile):\(line) (\(function))"
        
        let value: String
        if let obj = object {
            value = " -> \(obj)"
        } else {
            value = ""
        }

        print("\(Log.currentTimeStamp())\(prefixString): \(identifier)\(value)")
    }
    
    static func debug(_ object:Any? = nil, file: String = #file, functionName: String = #function, line: Int = #line) {
        Log.message(prefix: .debug, object: object,file: file, line: line, function: functionName)
    }
    
    static func thisFunction(file: String = #file, functionName: String = #function, line: Int = #line) {
        Log.message(prefix: .debug, file: file, line: line, function: functionName)
    }
    
    static func warning(_ object:Any? = nil, file: String = #file, functionName: String = #function, line: Int = #line) {
        Log.message(prefix: .warning, object: object, file: file, line: line, function: functionName)
    }
    
    static func error(_ object:Any? = nil, file: String = #file, functionName: String = #function, line: Int = #line) {
        Log.message(prefix: .error, object: object, file: file, line: line, function: functionName)
    }
}

