//
//  Views.swift
//  TheSet
//
//  Created by Evgeny Lee on 02.10.2020.
//

import RxSwift
import RxCocoa
import SnapKit

extension UIView {
    
    // MARK: - GestureRecognizers
    
    func add(target: Any?, action: Selector?) {
        isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: target, action: action)
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    func removeAllGestureRecognizers() {
        if let gRecognizers = gestureRecognizers {
            for recognizer in gRecognizers {
                removeGestureRecognizer(recognizer)
            }
        }
    }
    
    // MARK: - Subviews
    
    func fitView(_ view: UIView, inset: CGFloat = 0) {
        addSubview(view)
        view.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(inset)
        }
    }
    
    func fitView(_ view: UIView, edgeInsets: UIEdgeInsets) {
        addSubview(view)
        view.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(edgeInsets)
        }
    }
    
    func removeSubviews() {
        for view in subviews {
            view.removeFromSuperview()
        }
    }
    
    // MARK: - Shadow
    
    func setShadow(offset: CGSize, radius: CGFloat, opacity: Float, color: UIColor) {
        layer.masksToBounds = false
        layer.shadowRadius = radius
        layer.shadowOffset = offset
        layer.shadowOpacity = opacity
        layer.shadowColor = color.cgColor
    }
    
    // MARK: - Animation
    
    func animateConstraints() {
        UIView.animate(withDuration: 0.2) {[weak self] () in
            self?.layoutIfNeeded()
        }
    }
    
    // MARK: - Rendering
    
    func asImage() -> UIImage {
        let format = UIGraphicsImageRendererFormat()
        format.scale = UIScreen.main.scale
        let renderer = UIGraphicsImageRenderer(bounds: bounds, format: format)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}

extension Reactive where Base: UIView {
    private func tapGestureRecognizer() -> UITapGestureRecognizer {
        if let gestureRecognizers = self.base.gestureRecognizers {
            for gestureRecognizer in gestureRecognizers {
                if let recognizer = gestureRecognizer as? UITapGestureRecognizer {
                    return recognizer
                }
            }
        }
        
        let recognizer = UITapGestureRecognizer()
        self.base.addGestureRecognizer(recognizer)
        return recognizer
    }
    
    func tap() -> Observable<UIView> {
        self.base.isUserInteractionEnabled = true
        return tapGestureRecognizer().rx.event.map { _ in self.base }
    }
}
