//
//  Math.swift
//  TheSet
//
//  Created by Evgeny Lee on 05.10.2020.
//

import UIKit

struct Math {
    static func deg2rad(_ number: CGFloat) -> CGFloat {
        return number * .pi / 180
    }
    
    static func rad2deg(_ number: CGFloat) -> CGFloat {
        return number * 180 / .pi
    }
    
    static func generateCombinations(of count: Int, from variants: ClosedRange<Int>, noRepetitions: Bool = false) -> [[Int]] {
        return generateCombinations(of: count, from: Array(variants).map {[$0]}, noRepetitions: noRepetitions)
    }
    
    static func generateCombinations(of count: Int, from variants: [[Int]], noRepetitions: Bool = false) -> [[Int]] {
        var combinations = variants
        if count > 1  {
            let vars = generateCombinations(of: count - 1, from: variants, noRepetitions: noRepetitions)
            var coms = [[Int]]()
            combinations.forEach { c in
                vars.forEach { v in
                    let com = c + v
                    if noRepetitions {
                        if com.count == Set(com).count {
                            coms.append(com)
                        }
                    } else {
                        coms.append(com)
                    }
                }
            }
            combinations = coms
        }
        return combinations
    }
}
